<?php

// Activity

Class Person {
	
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName,$middleName,$lastName){

		// $this can reassign the value of function

		
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;


}
		public function printName(){
		return "Your Full name is $this->firstName $this->lastName";
	}
		

}


$man = new Person("Senku", " ", "Ishigami");


Class Developer {
	
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName,$middleName,$lastName){

		// $this can reassign the value of function

		
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;


}
		public function printName(){
		return "Your Full name is $this->firstName $this->lastName and you are a developer";
	}
		

}


$dev = new Developer("John Finch", " ", "Smith");



Class Engineer {
	
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName,$middleName,$lastName){

		// $this can reassign the value of function

		
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;


}
		public function printName(){
		return "Your are an engineer named $this->firstName $this->middleName $this->lastName";
	}
		

}


$eng = new Engineer("Harold", "Myers", "Reese");






