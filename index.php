<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity s03</title>
</head>
<body>
	<h1>Person</h1>
	<p><?= $man->printName(); ?></p>
	<h1>Developer</h1>
	<p><?= $dev->printName(); ?></p>	
	<h1>Engineer</h1>
	<p><?= $eng->printName(); ?></p>	

</body>
</html>